﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using System.Xml.Serialization;
using Hangman.Commands;
using Hangman.Helpers;
using Hangman.Models;
using Hangman.Services;

namespace Hangman.ViewModels
{
    [Serializable]
    public class UserViewModel : BaseViewModel
    {
        [XmlIgnore]
        private User _currentUser;
        private readonly string[] _images;
        private int _currentImagePosition;

        public UserViewModel()
        {
            Users = new ObservableCollection<User>();
            CurrentUser = new User { Name = "", ImagePath = "", Statistics = null };
            _images = Directory.GetFiles(Config.AvatarsPath);
        }

        public void Initialize()
        {
            UserViewModel entity = XmlSerializationHelpers.Deserialize<UserViewModel>(Config.ResourcesPath + "Users.xml");

            foreach (var item in entity.Users)
            {
                Users.Add(item);
            }
        }

        [XmlArray]
        public ObservableCollection<User> Users { get; set; }

        [XmlIgnore]
        public User CurrentUser {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                OnPropertyChanged(nameof(CurrentUser));
            }
        }

        [XmlIgnore]
        public string[] Images => _images;

        public int CurrentImagePosition
        {
            get { return _currentImagePosition; }
            set
            {
                _currentImagePosition = value;
                OnPropertyChanged(nameof(CurrentUser));
            }
        }

        [XmlIgnore]
        private bool _canExecuteCommand;
        public bool CanExecuteCommand
        {
            get
            {
                return _canExecuteCommand;
            }

            set
            {
                if (_canExecuteCommand)
                { return;}
                _canExecuteCommand = value;
            }
        }

        private ICommand _addUserCommand;
        public ICommand AddUserCommand
        {
            get
            {
                if (_addUserCommand == null)
                {
                    UserOperations operation = new UserOperations(this);
                    _addUserCommand = new RelayCommand(operation.AddUser);
                }
                return _addUserCommand;
            }
        }

        private ICommand _nextImageCommand;

        public ICommand NextUserImageCommandCommand
        {
            get
            {
                if (_nextImageCommand == null)
                {
                    UserOperations operation = new UserOperations(this);
                    _nextImageCommand = new RelayCommand(operation.NextUserImage, (IsUserSet));
                }
                return _nextImageCommand;
            }
        }

        private ICommand _prevImageCommand;

        public ICommand PrevUserImageCommand
        {
            get
            {
                if (_prevImageCommand == null)
                {
                    UserOperations operation = new UserOperations(this);
                    _prevImageCommand = new RelayCommand(operation.PrevUserImage, (IsUserSet));
                }
                return _prevImageCommand;
            }
        }

        private ICommand _deleteUserCommand;

        public ICommand DeleteUserCommand
        {
            get
            {
                if (_deleteUserCommand == null)
                {
                    UserOperations operation = new UserOperations(this);
                    _deleteUserCommand = new RelayCommand(operation.DeleteUser, (IsUserSet));
                }
                return _deleteUserCommand;
            }
        }

        private ICommand _playGameCommand;

        public ICommand PlayGameCommand
        {
            get
            {
                if (_playGameCommand == null)
                {
                    UserOperations operation = new UserOperations(this);
                    _playGameCommand = new RelayCommand(operation.PlayGame, (IsUserSet));
                }
                return _playGameCommand;
            }
        }

        private bool IsUserSet(object o)
        {
            if (CurrentUser != null && !CurrentUser.Name.Equals(""))
            {
                return true;
            }
            return false;
        }
    }
}
