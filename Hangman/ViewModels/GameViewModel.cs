﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Hangman.Commands;
using Hangman.Models;
using Hangman.Services;

namespace Hangman.ViewModels
{
    public class GameViewModel : BaseViewModel
    {
        private readonly GeometryGroup _support;
        private GeometryGroup _hangMan;
        private readonly Geometry[] _bodyParts = new Geometry[6];
        private List<char> _triedLetters;
        private GameData _gameData;
        private int _incorrectGuessesLeft;
        private Config.Categories _tempCategory;
        private ObservableCollection<Label> _wordLabels;
        //private System.Windows.Threading.DispatcherTimer _timer = new System.Windows.Threading.DispatcherTimer();

        public GameViewModel()
        {
            _gameData = new GameData();
            _support = new GeometryGroup();
            _hangMan = new GeometryGroup();
            _triedLetters = new List<char>();
            _incorrectGuessesLeft = Config.MaxWrongGuesses;
            _tempCategory = Config.Categories.None;
            _wordLabels = new ObservableCollection<Label>();
            GameData.Category = Config.Categories.None;

            //_timer.Tick += dispatcherTimer_Tick;
            //_timer.Interval = new TimeSpan(0, 0, 1);
            //_timer.Start();

            LineGeometry line1 = new LineGeometry(new Point(110, 200), new Point(190, 200));
            LineGeometry line2 = new LineGeometry(new Point(150, 200), new Point(150, 20));
            LineGeometry line3 = new LineGeometry(new Point(150, 24), new Point(250, 24));
            LineGeometry line4 = new LineGeometry(new Point(246, 24), new Point(245, 54));
            LineGeometry line5 = new LineGeometry(new Point(150, 60), new Point(180, 25));
            _support.Children.Add(line1);
            _support.Children.Add(line2);
            _support.Children.Add(line3);
            _support.Children.Add(line4);
            _support.Children.Add(line5);

            EllipseGeometry head = new EllipseGeometry(new Point(246, 70), 18, 18);
            LineGeometry body = new LineGeometry(new Point(246, 90), new Point(246, 150));
            LineGeometry leftArm = new LineGeometry(new Point(246, 110), new Point(216, 140));
            LineGeometry rightArm = new LineGeometry(new Point(246, 110), new Point(276, 140));
            LineGeometry leftLeg = new LineGeometry(new Point(246, 145), new Point(216, 180));
            LineGeometry rightLeg = new LineGeometry(new Point(246, 145), new Point(276, 180));
            _bodyParts[0] = head;
            _bodyParts[1] = body;
            _bodyParts[2] = leftArm;
            _bodyParts[3] = rightArm;
            _bodyParts[4] = leftLeg;
            _bodyParts[5] = rightLeg;
        }

        //private void dispatcherTimer_Tick(object sender, EventArgs e)
        //{
            
        //}

        //public System.Windows.Threading.DispatcherTimer Timer => _timer;

        public GameData GameData
        {
            get { return _gameData; }
            set
            {
                _gameData = value;
                OnPropertyChanged(nameof(GameData));
            }
        }
        
        public User CurrenUser { get; set; }

        public List<char> TriedLetters
        {
            get { return _triedLetters; }
            set
            {
                _triedLetters = value;
            }
        }
        public void ResetTriedLetters()
        {
            _triedLetters.Clear();
        }

        public ObservableCollection<Label> WordLabels
        {
            get
            {
                return _wordLabels;
            }

            set
            {
                _wordLabels = value;
            }
        }

        public void ResetWordLabels()
        {
            _wordLabels.Clear();
        }

        public int IncorrectGuessesLeft
        {
            get { return _incorrectGuessesLeft; }
            set
            {
                _incorrectGuessesLeft = value;
                OnPropertyChanged(nameof(IncorrectGuessesLeft));
            }
        }

        public Config.Categories TempCategory
        {
            get { return _tempCategory; }
            set
            {
                _tempCategory = value;
                OnPropertyChanged(nameof(TempCategory));
            }
        }

        public GeometryGroup Support => _support;

        public Geometry[] BodyParts => _bodyParts;

        public GeometryGroup HangMan
        {
            get { return _hangMan; }
            set
            {
                _hangMan = value;
                OnPropertyChanged(nameof(HangMan));
            }
        }

        public void ResetHangMan()
        {
            _hangMan.Children.Clear();
        }


        private ICommand _letterPressed;
        public ICommand LetterPressed
        {
            get
            {
                if (_letterPressed == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _letterPressed = new RelayCommand(operation.LetterPressed, LetterEnabler);
                }
                return _letterPressed;
            }
        }

        private ICommand _categoryChanged;
        public ICommand CategoryChanged
        {
            get
            {
                if (_categoryChanged == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _categoryChanged = new RelayCommand(operation.CategoryChanged);
                }
                return _categoryChanged;
            }
        }

        private ICommand _startGameCommand;
        public ICommand StartGameCommand
        {
            get
            {
                if (_startGameCommand == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _startGameCommand = new RelayCommand(operation.StartGame);
                }
                return _startGameCommand;
            }
        }

        private ICommand _saveGameCommand;
        public ICommand SaveGameCommand
        {
            get
            {
                if (_saveGameCommand == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _saveGameCommand = new RelayCommand(operation.SaveGame);
                }
                return _saveGameCommand;
            }
        }

        private ICommand _openGameCommand;
        public ICommand OpenGameCommand
        {
            get
            {
                if (_openGameCommand == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _openGameCommand = new RelayCommand(operation.OpenGame);
                }
                return _openGameCommand;
            }
        }

        private ICommand _helpCommand;
        public ICommand HelpCommand
        {
            get
            {
                if (_helpCommand == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _helpCommand = new RelayCommand(operation.Help);
                }
                return _helpCommand;
            }
        }

        private ICommand _statisticsCommand;
        public ICommand SatisticsCommand
        {
            get
            {
                if (_statisticsCommand == null)
                {
                    GameOperations operation = new GameOperations(this);
                    _statisticsCommand = new RelayCommand(operation.Statistics);
                }
                return _statisticsCommand;
            }
        }

        private bool LetterEnabler(object o)
        {
            if (GameData.Round == 0)
            {
                return false;
            }
            if (_triedLetters.Contains(o.ToString()[0]))
            {
                return false;
            }
            return true;
        }

    }
}