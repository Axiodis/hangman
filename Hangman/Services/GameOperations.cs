﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Hangman.Helpers;
using Hangman.Models;
using Hangman.ViewModels;
using Microsoft.Win32;

namespace Hangman.Services
{
    class GameOperations
    {
        private readonly GameViewModel _gameViewModel;

        public GameOperations(GameViewModel gameViewModel)
        {
            _gameViewModel = gameViewModel;
        }

        public void CategoryChanged(object param)
        {
            Config.Categories category;

            Enum.TryParse(param.ToString(), out category);

            if (category == _gameViewModel.TempCategory)
            {
                _gameViewModel.TempCategory = Config.Categories.None;
            }
            else
            {
                _gameViewModel.TempCategory = category;
            }
            
        }

        public void StartGame(object param)
        {
            _gameViewModel.GameData.Category = _gameViewModel.TempCategory;
            if (_gameViewModel.GameData.Category != Config.Categories.None)
            {

                if (_gameViewModel.GameData.Round > 0)
                {
                    MessageBoxResult message = MessageBox.Show("Game Lost!");
                    _gameViewModel.CurrenUser.Statistics[_gameViewModel.GameData.Category.ToString()].TotalGames++;
                }

                _gameViewModel.GameData.Round = 1;
                SetUpRound();
            }
            else
            {
                MessageBoxResult message = MessageBox.Show("Please Choose a Category");
            }
        }

        private void SetUpRound()
        {
            ResetGame();

            Random random = new Random();

            string fileName = _gameViewModel.GameData.Category.ToString() + ".txt";

            int numberOfWords = File.ReadLines(Config.WordsPath + fileName).Count();

            int randomNumber = random.Next(0, numberOfWords);
            _gameViewModel.GameData.Word = File.ReadLines(Config.WordsPath + fileName)
                .Skip(randomNumber)
                .Take(1)
                .First();

            MakeLabels(_gameViewModel.GameData.Word);
        }

        private void ResetGame()
        {

            _gameViewModel.GameData.Guesses = 0;
            _gameViewModel.GameData.Mistakes = 0;
            _gameViewModel.IncorrectGuessesLeft = Config.MaxWrongGuesses;

            _gameViewModel.ResetTriedLetters();

            _gameViewModel.ResetHangMan();
        }

        private void MakeLabels(string currentWord, List<char> guessedLetters = null)
        {

            _gameViewModel.ResetWordLabels();
            char[] chars = currentWord.ToCharArray();
            double spcBetween = (400 - 10) / chars.Length;
            for (int i = 0; i < chars.Length; i++)
            {
                Label chrLabel = new Label
                {
                    Margin = new Thickness((i * spcBetween) + 10, 60 / 2 - 20, 0, 0),
                    FontSize = 20,
                    Content = "_"
                };

                if (guessedLetters != null)
                {
                    foreach (char guessedLetter in guessedLetters)
                    {
                        if (guessedLetter == currentWord[i])
                        {
                            chrLabel.Content = currentWord[i];
                        }
                    }
                }

                _gameViewModel.WordLabels.Add(chrLabel);
            }
        }

        public void LetterPressed(object param)
        {
            char letter = param.ToString()[0];

            _gameViewModel.TriedLetters.Add(letter);

            Solve(letter);

            CheckEndGame();
        }

        private void Solve(char letter)
        {
            int count = 0;
            for (int i = 0; i < _gameViewModel.GameData.WordLength; i++)
            {
                char ch = _gameViewModel.GameData.Word[i];
                if (letter.Equals(ch))
                {
                    _gameViewModel.GameData.Guesses++;
                    count++;
                }
            }
            if (count != 0)
            {
                UpdateWordLabels(letter);
                _gameViewModel.GameData.Letters.Add(letter);
            }
            else
            {
                AddShape(_gameViewModel.GameData.Mistakes);
                _gameViewModel.IncorrectGuessesLeft--;
                _gameViewModel.GameData.Mistakes++;
            }
        }

        private void AddShape(int part)
        {
            _gameViewModel.HangMan.Children.Add(_gameViewModel.BodyParts[part]);
        }

        private void UpdateWordLabels(char letter)
        {
            if (_gameViewModel.GameData.Word.Contains(letter))
            {
                char[] wordLetters = _gameViewModel.GameData.Word.ToCharArray();
                for (int i = 0; i < wordLetters.Length; i++)
                {
                    if (wordLetters[i] == letter)
                    {
                        _gameViewModel.WordLabels[i].Content = letter.ToString();
                    }
                }
                foreach (Label label in _gameViewModel.WordLabels)
                {
                    if (label.Content.Equals("_")) return;
                }
            }
        }

        private void CheckEndGame()
        {
            if (_gameViewModel.GameData.Guesses == _gameViewModel.GameData.WordLength)
            {
                if (_gameViewModel.GameData.Round < 5)
                {
                    MessageBoxResult message = MessageBox.Show("Round " + _gameViewModel.GameData.Round + " Won!");
                    _gameViewModel.GameData.Round++;
                    SetUpRound();
                }
                else
                {
                    MessageBoxResult message = MessageBox.Show("Game Won!");
                    _gameViewModel.CurrenUser.Statistics[_gameViewModel.GameData.Category.ToString()].TotalGames++;
                    _gameViewModel.CurrenUser.Statistics[_gameViewModel.GameData.Category.ToString()].Wins++;
                    _gameViewModel.GameData.Round = 0;
                }
            }
            else if (_gameViewModel.GameData.Mistakes == Config.MaxWrongGuesses)
            {
                MessageBoxResult message = MessageBox.Show("Game Lost!");
                _gameViewModel.CurrenUser.Statistics[_gameViewModel.GameData.Category.ToString()].TotalGames++;
                _gameViewModel.GameData.Round = 0;
            }
        }

        public void SaveGame(object param)
        {
            SaveFileDialog dlg = new SaveFileDialog
            {
                FileName = "New Save",
                DefaultExt = ".xml",
                Filter = "Save game (.xml)|*.xml",
                InitialDirectory = Config.SavesPath
            };

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                XmlSerializationHelpers.Serialize(filename, _gameViewModel.GameData);
            }
        }

        public void OpenGame(object param)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Filter = "Save game (.xml)|*.xml",
                InitialDirectory = Config.SavesPath
            };

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                _gameViewModel.GameData = XmlSerializationHelpers.Deserialize<GameData>(filename);
            }

            for (int i = 0; i < _gameViewModel.GameData.Mistakes; i++)
            {
                AddShape(i);
            }

            _gameViewModel.IncorrectGuessesLeft = Config.MaxWrongGuesses - _gameViewModel.GameData.Mistakes;
            MakeLabels(_gameViewModel.GameData.Word, _gameViewModel.GameData.Letters);
        }

        public void Help(object param)
        {
            MessageBox.Show("Martin Alexandru\n 10LF352\n Info Aplicata", "Help!");
        }

        public void Statistics(object param)
        {
            MessageBox.Show(_gameViewModel.CurrenUser.ShowStatistics(), "Your Statistics");
        }
    }
}
