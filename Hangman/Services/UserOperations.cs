﻿using System;
using System.Linq;
using System.Windows;
using Hangman.Helpers;
using Hangman.Models;
using Hangman.ViewModels;
using Hangman.Views;

namespace Hangman.Services
{
    class UserOperations
    {
        private readonly UserViewModel _userViewModel;

        public UserOperations(UserViewModel userViewModel)
        {
            _userViewModel = userViewModel;
        }

        public int CurrentImagePosition()
        {
            int currentImagePosition = 0;

            for (int i = 0; i < _userViewModel.Images.Length; i++)
            {
                if (_userViewModel.CurrentUser != null && _userViewModel.CurrentUser.ImagePath.Equals(_userViewModel.Images[i]))
                {
                    currentImagePosition = i;
                    break;
                }
            }

            return currentImagePosition;
        }

        public void AddUser(object param)
        {
            String newUsername = Microsoft.VisualBasic.Interaction.InputBox("Username", "New User");
            if (!newUsername.Equals(""))
            {
                User newUser = new User(newUsername);
                UserNameComparator comp = new UserNameComparator();

                if (!_userViewModel.Users.Contains(newUser, comp))
                {
                    _userViewModel.Users.Add(newUser);
                }
                else
                {
                    MessageBox.Show("Username taken!");
                }
            }
            XmlSerializationHelpers.Serialize(Config.ResourcesPath + "Users.xml", _userViewModel);
        }

        public void DeleteUser(object param)
        {
            _userViewModel.Users.Remove(_userViewModel.Users.FirstOrDefault(i => i.Name == _userViewModel.CurrentUser.Name));

            XmlSerializationHelpers.Serialize(Config.ResourcesPath + "Users.xml", _userViewModel);
        }

        public void NextUserImage(object param)
        {
            _userViewModel.CurrentImagePosition = CurrentImagePosition();

            if ((_userViewModel.CurrentImagePosition + 1) < _userViewModel.Images.Length)
            {
                _userViewModel.CurrentImagePosition++;
                _userViewModel.CurrentUser.ImagePath = _userViewModel.Images[_userViewModel.CurrentImagePosition];
            }
            else
            {
                _userViewModel.CurrentImagePosition = 0;
                _userViewModel.CurrentUser.ImagePath = _userViewModel.Images[_userViewModel.CurrentImagePosition];
            }

            XmlSerializationHelpers.Serialize(Config.ResourcesPath + "Users.xml", _userViewModel);
        }

        public void PrevUserImage(object param)
        {
            _userViewModel.CurrentImagePosition = CurrentImagePosition();

            if ((_userViewModel.CurrentImagePosition - 1) >= 0)
            {
                _userViewModel.CurrentImagePosition--;
                _userViewModel.CurrentUser.ImagePath = _userViewModel.Images[_userViewModel.CurrentImagePosition];
            }
            else
            {
                _userViewModel.CurrentImagePosition = _userViewModel.Images.Length - 1;
                _userViewModel.CurrentUser.ImagePath = _userViewModel.Images[_userViewModel.CurrentImagePosition];
            }


            XmlSerializationHelpers.Serialize(Config.ResourcesPath + "Users.xml", _userViewModel);
        }

        public void PlayGame(object param)
        {
            var gameWindow = new GameWindow(_userViewModel.CurrentUser);
            gameWindow.Show();
            Application.Current.MainWindow.Hide();
        }
    }
}
