﻿using System.ComponentModel;
using System.Windows;
using Hangman.Models;
using Hangman.ViewModels;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class GameWindow : Window
    {
        public GameWindow( User currentUser)
        {
            InitializeComponent();
            ((GameViewModel) Resources["GameViewModel"]).CurrenUser = currentUser;
        }
        protected override void OnClosing(CancelEventArgs cancelEventArgs)
        {
            Application.Current.MainWindow.Show();
        }
    }
}