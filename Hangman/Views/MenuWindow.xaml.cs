﻿using System.ComponentModel;
using System.Windows;
using Hangman.Helpers;
using Hangman.ViewModels;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            InitializeComponent();
            ((UserViewModel)Resources["UserViewModel"]).Initialize();
        }

        protected override void OnClosing(CancelEventArgs cancelEventArgs)
        {
            XmlSerializationHelpers.Serialize(Config.ResourcesPath + "Users.xml", (UserViewModel)Resources["UserViewModel"]);
        }
    }
}
