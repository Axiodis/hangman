﻿using System;
using System.Windows.Input;

namespace Hangman.Commands
{
    class RelayCommand : ICommand
    {
        private readonly Action<object> _commandTask;
        private readonly Predicate<object> _canExecuteTask;

        public RelayCommand(Action<object> workToDo)
            : this(workToDo, DefaultCanExecute)
        {
            _commandTask = workToDo;
        }

        public RelayCommand(Action<object> workToDo, Predicate<object> canExecute)
        {
            _commandTask = workToDo;
            _canExecuteTask = canExecute;
        }

        private static bool DefaultCanExecute(object parameter)
        {
            return true;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecuteTask != null && _canExecuteTask(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _commandTask(parameter);
        }
    }
}
