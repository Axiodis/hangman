﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Hangman.Annotations;

namespace Hangman.Models
{
    [Serializable]
    public class GameData : INotifyPropertyChanged
    {
        private int _round;
        private int _mistakes;
        private int _correctGuesses;
        private int _wordLength;
        private string _currentWord;
        private List<char> _guesedLetters;
        private Config.Categories _category;
        private static GameData _instance;

        public GameData()
        {
            _category = Config.Categories.None;
            _guesedLetters = new List<char>();
        }

        //public static GameData Instance => _instance ?? (_instance = new GameData());


        [XmlAttribute]
        public int Round
        {
            get { return _round; }
            set
            {
                _round = value;
                OnPropertyChanged(nameof(Round));
            }
        }

        [XmlAttribute]
        public int Mistakes
        {
            get { return _mistakes; }
            set
            {
                _mistakes = value;
                OnPropertyChanged(nameof(Mistakes));
            }
        }

        [XmlAttribute]
        public int Guesses
        {
            get { return _correctGuesses; }
            set
            {
                _correctGuesses = value;
                OnPropertyChanged(nameof(Guesses));
            }
        }

        [XmlAttribute]
        public string Word
        {
            get { return _currentWord; }
            set
            {
                _currentWord = value;
                _wordLength = value.Length;
                OnPropertyChanged(nameof(Word));
            }
        }

        [XmlAttribute]
        public int WordLength
        {
            get { return _wordLength; }
            set
            {
                _wordLength = value;
                OnPropertyChanged(nameof(WordLength));
            }
        }

        [XmlAttribute]
        public List<char> Letters
        {
            get { return _guesedLetters; }
            set
            {
                _guesedLetters = value;
                OnPropertyChanged(nameof(Letters));
            }
        }

        [XmlAttribute]
        public Config.Categories Category
        {
            get { return _category; }
            set
            {
                _category = value;
                OnPropertyChanged(nameof(Category));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
