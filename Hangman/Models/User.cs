﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Hangman.Annotations;
using Xpand.Utils.GeneralDataStructures;

namespace Hangman.Models
{
    [Serializable]
    public class User : INotifyPropertyChanged
    {

        private string _name;
        private string _imagePath;
        public User()
        {
            Statistics = new SerializableDictionary<string, Statistic>();

            ImagePath = Config.AvatarsPath + "Default.png";

            foreach (string category in Enum.GetNames(typeof(Config.Categories)))
            {
                if (!category.Equals("None"))
                {
                    Statistics[category] = new Statistic();
                }
            }
        }

        public string ShowStatistics()
        {
            string allStatistics = "";
            foreach (KeyValuePair<string, Statistic> statistic in Statistics)
            {
                allStatistics += statistic.Key + "\n Games: " + statistic.Value.TotalGames + "\n Games Won: " +
                                 statistic.Value.Wins + "\n \n";
            }
            return allStatistics;
        }

        public User(string name) : this()
        {
            Name = name;
        }

        [XmlAttribute]
        public string Name
        {
            get{ return _name; }
            set{
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        [XmlAttribute]
        public string ImagePath
        {
            get{ return _imagePath; }
            set{
                _imagePath = value;
                OnPropertyChanged(nameof(ImagePath));
            }
        }


        public SerializableDictionary<string, Statistic> Statistics { get; set; }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
