﻿using System;
using System.Xml.Serialization;

namespace Hangman.Models
{
    [Serializable]
    public class Statistic
    {
        public Statistic()
        {
            Wins = 0;
            TotalGames = 0;
        }

        [XmlAttribute]
        public int Wins { get; set; }

        [XmlAttribute]
        public int TotalGames { get; set; }
    }
}
