﻿using System;
using System.Collections.Generic;
using Hangman.Models;

namespace Hangman.Helpers
{
    class UserNameComparator : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the users' properties are equal.
            return x.Name.Equals(y.Name);
        }

        public int GetHashCode(User user)
        {
            throw new NotImplementedException();
        }
    }
}
