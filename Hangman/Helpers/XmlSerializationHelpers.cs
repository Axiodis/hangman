﻿using System.IO;
using System.Xml.Serialization;

namespace Hangman.Helpers
{
    public class XmlSerializationHelpers
    {

        public static void Serialize<T>(string xmlFileName, T entity)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(T));
            using (FileStream fileStr = new FileStream(xmlFileName, FileMode.Create))
            {
                xmlser.Serialize(fileStr, entity);
            }
        }

        public static T Deserialize<T>(string xmlFileName)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(T));
            using (FileStream file = new FileStream(xmlFileName, FileMode.Open))
            {
                var entity = xmlser.Deserialize(file);
                return (T)entity;
            }
        }
    }
}
