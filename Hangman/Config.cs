﻿using System.IO;

namespace Hangman
{
    public static class Config
    {
        public enum Categories { All, Movies, Cars, None }

        public const int RoundsToWin = 5;
        public const int MaxWrongGuesses = 6;
        public static string ResourcesPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + "\\Resources\\";
        public static string AvatarsPath = ResourcesPath + "Avatars\\";
        public static string WordsPath = ResourcesPath + "Words\\";
        public static string SavesPath = ResourcesPath + "Saves\\";
    }
}
